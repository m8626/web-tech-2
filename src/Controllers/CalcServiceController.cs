﻿using System;
using app.Models;
using Microsoft.AspNetCore.Mvc;


namespace app.Controllers {
    public class CalcServiceController : Controller {

        public IActionResult Index() {
            return View();
        }



        public IActionResult ManualParsingSingle() {
            if (Request.Method == System.Net.WebRequestMethods.Http.Post)
            {
                try
                {
                    var calc = new CalcModel
                    {
                        X = Int32.Parse(HttpContext.Request.Form["x"]),
                        Y = Int32.Parse(HttpContext.Request.Form["y"]),
                        Operation = HttpContext.Request.Form["operation"],
                    };

                    ViewBag.Result = calc.Calc();
                }
                catch
                {
                    ViewBag.Result = "Invalid input";
                }

                return View("Result");
            }
            return View("Form");
        }



        [HttpGet]
        [ActionName("ManualParsingSeparate")]
        public IActionResult ManualParsingSeparateGet()
        {
            return View("Form");
        }
        [HttpPost]
        [ActionName("ManualParsingSeparate")]
        public IActionResult ManualParsingSeparatePost() {
            try
            {
                var calc = new CalcModel
                {
                    X = Int32.Parse(HttpContext.Request.Form["x"]),
                    Y = Int32.Parse(HttpContext.Request.Form["y"]),
                    Operation = HttpContext.Request.Form["operation"],
                };

                ViewBag.Result = calc.Calc();
            }
            catch
            {
                ViewBag.Result = "Invalid input";
            }

            return View("Result");
        }



        [HttpGet]
        public IActionResult ModelBindingParameters()
        {
            return View("Form");
        }
        [HttpPost]
        public IActionResult ModelBindingParameters(int x, string operation, int y)
        {
            if (ModelState.IsValid)
            {
                var calc = new CalcModel
                {
                    X = x,
                    Y = y,
                    Operation = operation,
                };
                ViewBag.Result = calc.Calc();
            }
            else
            {
                ViewBag.Result = "Invalid input";
            }

            return View("Result");
        }



        [HttpGet]
        public IActionResult ModelBindingSeparate()
        {
            return View("Form");
        }
        [HttpPost]
        public IActionResult ModelBindingSeparate(CalcModel model)
        {
            ViewBag.Result = ModelState.IsValid
                ? model.Calc()
                : "Invalid input";

            return View("Result");
        }
    }
}
